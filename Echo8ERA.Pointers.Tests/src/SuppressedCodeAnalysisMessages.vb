Imports System.Diagnostics.CodeAnalysis

<Assembly: SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames",
		Justification := "The project's purpose is to implement pointers",
		Scope := "Module")>
<Assembly: SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores",
		Justification := "Underscores in identfier names in unit tests is a thing",
		Scope := "Module")>
<Assembly: SuppressMessage("Microsoft.Naming", "CA1052:StaticHolderTypesShouldBeSealed",
		Justification := "The shared members are supposed to only be seen by derived classes",
		Target := "Echo8ERA.Pointers.Tests.PointerTestBase",
		Scope := "Type")>
