Imports Echo8ERA.Pointers
Imports NUnit.Framework

Namespace Echo8ERA.Pointers.Tests

	<TestFixture>
	Public Class Test_Casts_Pointer_VoidPointer
		Inherits PointerTestBase

		<SetUp>
		Public Sub Setup()
		End Sub

		<Test>
		Public Sub CastPointerToVoidPointer_HasSameValue()

			' Arrange
			Dim Ptr = New Pointer(Of Integer)(New IntPtr(&HDEADBEEF))

			' Act
			Dim Res = CType(Ptr, VoidPointer)

			' Assert
			Assert.That(PointerTestBase.get_m_Pointer(Res), [Is].EqualTo(PointerTestBase.get_m_Pointer(Ptr)))

		End Sub

		<Test>
		Public Sub CastVoidPointerToPointer_HasSameValue()

			' Arrange
			Dim Ptr = New VoidPointer(New IntPtr(&HDEADBEEF))

			' Act
			Dim Res = CType(Ptr, Pointer(Of Integer))

			' Assert
			Assert.That(PointerTestBase.get_m_Pointer(Res), [Is].EqualTo(PointerTestBase.get_m_Pointer(Ptr)))

		End Sub

	End Class

End Namespace