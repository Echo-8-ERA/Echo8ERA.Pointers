Imports SuppressMessage = System.Diagnostics.CodeAnalysis.SuppressMessageAttribute
Imports System.Reflection
Imports System.Reflection.Emit
Imports Echo8ERA.Pointers
Imports NUnit.Framework

Namespace Echo8ERA.Pointers.Tests

	''' <summary>
	''' Base class for unit testing the <see cref="Pointer(Of T)" /> and
	''' <see cref="VoidPointer" /> classes.
	''' </summary>
	Public MustInherit class PointerTestBase

		Private Delegate Function AsPointerFunc(Of T)(ByRef arg As T) As IntPtr

		Private Shared s_GetField_m_Pointer As New Dictionary(Of Type, [Delegate])()
		Private Shared s_Call_AsPointer As New Dictionary(Of Type, [Delegate])()
		Private Shared ReadOnly s_IntPtrCtor = GetType(IntPtr).GetConstructor(
						BindingFlags.Instance Or BindingFlags.Public,
						Nothing,
						New Type() { GetType(Void).MakePointerType()},
						Nothing)

		Private Shared Function Internal_get_m_Pointer(Of T)(ByVal ptr As T) As IntPtr

			Dim PtrType = GetType(T)

			If Not s_GetField_m_Pointer.ContainsKey(PtrType) Then

				Dim FldInfo = PtrType.GetField("m_Pointer", BindingFlags.Instance Or BindingFlags.NonPublic)

				Dim DynMthd = New DynamicMethod(String.Empty, GetType(IntPtr), New Type() { PtrType }, True)
				With DynMthd.GetILGenerator()
					.Emit(OpCodes.Ldarg_0)
					.Emit(OpCodes.Ldfld, FldInfo)
					.Emit(OpCodes.Newobj, PointerTestBase.s_IntPtrCtor)
					.Emit(OpCodes.Ret)
				End With
				s_GetField_m_Pointer.Add(PtrType, DynMthd.CreateDelegate(GetType(Func(Of T, IntPtr))))

			End If

			Return DirectCast(s_GetField_m_Pointer(PtrType), Func(Of T, IntPtr))(ptr)

		End Function

		''' <summary>
		''' Gets the private <c>m_Pointer</c> field of the <see cref="Pointer(Of T)" /> structure.
		''' </summary>
		''' <param name="ptr">
		''' The <see cref="Pointer(Of T)" /> to get the private <c>m_Pointer</c> field of.
		''' </param>
		''' <returns>
		''' The private <c>m_Pointer</c> of the <see cref="Pointer(Of T)" /> structure as an
		''' <see cref="IntPtr" />.
		''' </returns>
		Protected Shared Function get_m_Pointer(Of T As Structure)(ByVal ptr As Pointer(Of T)) As IntPtr

			Return PointerTestBase.Internal_get_m_Pointer(ptr)

		End Function

		''' <summary>
		''' Gets the private <c>m_Pointer</c> field of the <see cref="VoidPointer" /> structure.
		''' </summary>
		''' <param name="ptr">
		''' The <see cref="VoidPointer" /> to get the private <c>m_Pointer</c> field of.
		''' </param>
		''' <returns>
		''' The private <c>m_Pointer</c> of the <see cref="VoidPointer" /> structure as an
		''' <see cref="IntPtr" />.
		''' </returns>
		Protected Shared Function get_m_Pointer(ByVal ptr As VoidPointer) As IntPtr

			Return PointerTestBase.Internal_get_m_Pointer(ptr)

		End Function

		''' <summary>
		''' Gets the address of a variable using the
		''' <see cref="System.Runtime.CompilerServices.Unsafe.AsPointer" /> method.
		''' </summary>
		''' <param name="o">
		''' The variable to get the address of.
		''' </param>
		''' <returns>
		''' The address of <paramref name="o" /> as an <see cref="IntPtr" />.
		''' </returns>
		Protected Shared Function AsPointer(Of T As Structure)(ByRef o As T) As IntPtr

			Dim ValType = GetType(T)
			If Not s_Call_AsPointer.ContainsKey(ValType) Then

				Dim MthdInfo = GetType(System.Runtime.CompilerServices.Unsafe) _
						.GetMethod("AsPointer") _
						.MakeGenericMethod(ValType)

				Dim DynMthd = New DynamicMethod(String.Empty, GetType(IntPtr), New Type() { ValType.MakeByRefType() }, True)
				With DynMthd.GetILGenerator()
					.Emit(OpCodes.Ldarg_0)
					.Emit(OpCodes.Call, MthdInfo)
					.Emit(OpCodes.Newobj, s_IntPtrCtor)
					.Emit(OpCodes.Ret)
				End With

				s_Call_AsPointer.Add(ValType, DynMthd.CreateDelegate(GetType(AsPointerFunc(Of T))))

			End If

			Return DirectCast(s_Call_AsPointer(ValType), AsPointerFunc(Of T))(o)

		End Function

	End Class

End Namespace