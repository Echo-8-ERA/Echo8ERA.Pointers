Option Explicit On
Option Infer On
Option Strict On

Imports System.Runtime.CompilerServices
Imports Echo8ERA.Pointers
Imports NUnit.Framework

Namespace Echo8ERA.Pointers.Tests

	<TestFixture>
	Public Class Test_PointerHelpers
		Inherits PointerTestBase

		<SetUp>
		Public Sub Setup()
		End Sub

		<Test>
		Public Sub TakeAddressOfLocalVar_HasSameValueAsUnsafeClass()

			' Arrange
			Dim LocVar = 42
			Dim Address = PointerTestBase.AsPointer(LocVar)

			' Act
			Dim Res = [AddressOf](LocVar)

			' Assert
			Assert.That(PointerTestBase.get_m_Pointer(Res), [Is].EqualTo(Address))

		End Sub

		<Test>
		Public Sub ReadDereferencedPointer_HasSameValue()

			' Arrange
			Dim LocVar = 42
			Dim Ptr = [AddressOf](LocVar)

			' Act
			Dim Res = DeRef(Ptr)

			' Assert
			Assert.That(Res, [Is].EqualTo(LocVar))

		End Sub

		<Test>
		Public Sub SetDereferencedPointer_HasNewValue()

			' Arrange
			Dim LocVar = 42
			Dim Ptr = [AddressOf](LocVar)

			' Act
			DeRef(Ptr) = 420

			' Assert
			Assert.That(LocVar, [Is].EqualTo(420))

		End Sub

	End Class

End Namespace