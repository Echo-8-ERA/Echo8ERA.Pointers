Imports Echo8ERA.Pointers
Imports NUnit.Framework

Namespace Echo8ERA.Pointers.Tests

	<TestFixture>
	Public Class Test_Casts_Pointer_IntPtr
		Inherits PointerTestBase

		<SetUp>
		Public Sub Setup()
		End Sub

		<Test>
		Public Sub CastIntPtrToPointer_HasSameValue()

			' Arrange
			Dim Val = New IntPtr(&HDEADBEEF)

			' Act
			Dim Res = CType(Val, Pointer(Of Integer))

			' Assert
			Assert.That(PointerTestBase.get_m_Pointer(Res), [Is].EqualTo(Val))

		End Sub

		<Test>
		Public Sub CastPointerToIntPtr_HasSameValue()

			' Arrange
			Dim Val = New IntPtr(&HDEADBEEF)
			Dim Ptr = New Pointer(Of Integer)(Val)

			' Act
			Dim Res = CType(Ptr, IntPtr)

			' Assert
			Assert.That(Res, [Is].EqualTo(Val))

		End Sub

	End Class

End Namespace