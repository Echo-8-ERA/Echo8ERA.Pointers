Imports Echo8ERA.Pointers
Imports NUnit.Framework

Namespace Echo8ERA.Pointers.Tests

	<TestFixture>
	Public Class Test_Casts_VoidPointer_IntPtr
		Inherits PointerTestBase

		<SetUp>
		Public Sub Setup()
		End Sub

		<Test>
		Public Sub CastIntPtrToVoidPointer_HasSameValue()

			' Arrange
			Dim Val = New IntPtr(&HDEADBEEF)

			' Act
			Dim Res = CType(Val, VoidPointer)

			' Assert
			Assert.That(PointerTestBase.get_m_Pointer(Res), [Is].EqualTo(Val))

		End Sub

		<Test>
		Public Sub CastVoidPointerToIntPtr_HasSameValue()

			' Arrange
			Dim Val = New IntPtr(&HDEADBEEF)
			Dim Ptr = New VoidPointer(Val)

			' Act
			Dim Res = CType(Ptr, IntPtr)

			' Assert
			Assert.That(Res, [Is].EqualTo(Val))

		End Sub

	End Class

End Namespace