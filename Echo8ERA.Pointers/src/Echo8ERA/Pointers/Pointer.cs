using System;

using InlineIL;
using static InlineIL.IL.Emit;

namespace Echo8ERA.Pointers {

	// TODO: Change to T : unmanaged when VB.Net supports the unmanaged constraint
	/// <summary>
	/// A type that is used to represent a typed pointer.
	/// </summary>
	/// <seealso cref="VoidPointer" />
	public unsafe class Pointer<T> where T : struct {

		private void *m_Pointer;

		internal Pointer(void *ptr) => this.m_Pointer = ptr;

		/// <summary>
		/// Initializes a new instance of <see cref="Pointer&lt;T&gt;" /> using the
		/// specified <see cref="System.IntPtr" />.
		/// </summary>
		/// <param name="ptr">
		/// A pointer or handle to encapsulate.
		/// </param>
		public Pointer(IntPtr ptr) : this(ptr.ToPointer()) { }

		/// <summary>
		/// Converts the specified <see cref="Pointer&lt;T&gt;" /> into a
		/// <see cref="System.IntPtr" />.
		/// </summary>
		/// <param name="ptr">
		/// The pointer or handle to convert.
		/// </param>
		/// <returns>
		/// A new instance of <see cref="System.IntPtr" /> initialized to
		/// <paramref name="ptr" />.
		/// </returns>
		public static explicit operator IntPtr(Pointer<T> ptr) => new IntPtr(ptr.m_Pointer);

		/// <summary>
		/// Converts the specified <see cref="System.IntPtr" /> into a
		/// <see cref="Pointer&lt;T&gt;" />.
		/// </summary>
		/// <param name="ptr">
		/// The pointer or handle to convert.
		/// </param>
		/// <returns>
		/// A new instance of <see cref="Pointer&lt;T&gt;" /> initialized to
		/// <paramref name="ptr" />.
		/// </returns>
		public static explicit operator Pointer<T>(IntPtr ptr) => new Pointer<T>(ptr);

		/// <summary>
		/// Dereferences a <see cref="Pointer&lt;T&gt;" />.
		/// </summary>
		/// <returns>
		/// A reference to the <see cref="System.ValueType" /> pointed to by the
		/// <see cref="Pointer&lt;T&gt;" />.
		/// </returns>
		internal ref T DeRef() {

			Ldarg_0();
			Ldfld(new FieldRef(typeof(Pointer<T>), nameof(m_Pointer)));
			Ret();

			throw IL.Unreachable();

		}

		/// <summary>
		/// Converts the specified <see cref="Pointer&lt;T&gt;" /> into a
		/// <see cref="VoidPointer" />.
		/// </summary>
		/// <param name="ptr">
		/// The pointer or handle to convert.
		/// </param>
		/// <returns>
		/// A new instance of <see cref="VoidPointer" /> initialized to
		/// <paramref name="ptr" />.
		/// </returns>
		public static implicit operator VoidPointer(Pointer<T> ptr) => new VoidPointer(ptr.m_Pointer);

		/// <summary>
		/// Converts the specified <see cref="VoidPointer" /> into a
		/// <see cref="Pointer&lt;T&gt;" />.
		/// </summary>
		/// <param name="ptr">
		/// The pointer or handle to convert.
		/// </param>
		/// <returns>
		/// A new instance of <see cref="VoidPointer" /> initialized to
		/// <paramref name="ptr" />.
		/// </returns>
		public static explicit operator Pointer<T>(VoidPointer ptr) => new Pointer<T>(ptr.m_Pointer);

	}

}