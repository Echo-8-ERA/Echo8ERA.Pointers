using System;

using InlineIL;
using static InlineIL.IL.Emit;

namespace Echo8ERA.Pointers {

	/// <summary>
	/// Represents a pointer of unspecified type.
	/// </summary>
	/// <seealso cref="Pointer&lt;T&gt;" />
	public unsafe class VoidPointer {

		/// <summary>
		/// The pointer that the <see cref="VoidPointer" /> represents.
		/// </summary>
		internal void *m_Pointer;

		/// <summary>
		/// Constructs a new instance of <see cref="VoidPointer" /> from a
		/// <c><see cref="System.Void" /> *</c>.
		/// </summary>
		/// <param name="ptr">
		/// A pointer or handle to encapsulate.
		/// </param>
		internal VoidPointer(void *ptr) => this.m_Pointer = ptr;

		/// <summary>
		/// Initializes a new instance of <see cref="VoidPointer" /> using the
		/// specified <see cref="System.IntPtr" />.
		/// </summary>
		/// <param name="ptr">
		/// A pointer or handle to encapsulate.
		/// </param>
		public VoidPointer(IntPtr ptr) : this(ptr.ToPointer()) { }

		/// <summary>
		/// Converts the specified <see cref="VoidPointer" /> into a
		/// <see cref="System.IntPtr" />.
		/// </summary>
		/// <param name="ptr">
		/// The pointer or handle to convert.
		/// </param>
		/// <returns>
		/// A new instance of <see cref="System.IntPtr" /> initialized to
		/// <paramref name="ptr" />.
		/// </returns>
		public static explicit operator IntPtr(VoidPointer ptr) => new IntPtr(ptr.m_Pointer);

		/// <summary>
		/// Converts the specified <see cref="System.IntPtr" /> into a
		/// <see cref="VoidPointer" />.
		/// </summary>
		/// <param name="ptr">
		/// The pointer or handle to convert.
		/// </param>
		/// <returns>
		/// A new instance of <see cref="VoidPointer" /> initialized to
		/// <paramref name="ptr" />.
		/// </returns>
		public static explicit operator VoidPointer(IntPtr ptr) => new VoidPointer(ptr);

	}

}