using System;
using Microsoft.VisualBasic.CompilerServices;

using InlineIL;
using static InlineIL.IL.Emit;

namespace Echo8ERA.Pointers {

	/// <summary>
	/// Provides a set of <c>static</c> (<c>Shared</c> in Visual Basic) methods
	/// for working with <see cref="Pointer&lt;T&gt;" /> and <see cref="VoidPointer" />
	/// </summary>
	[StandardModule]
	public unsafe static class PointerHelpers {

		// TODO: Change to T : unmanaged when VB.Net supports the unmanaged constraint
		/// <summary>
		/// Dereferences a <see cref="Pointer&lt;T&gt;" />.
		/// </summary>
		/// <param name="ptr">
		/// The <see cref="Pointer&lt;T&gt;" /> to dereference.
		/// </param>
		/// <returns>
		/// A reference to the <see cref="System.ValueType" /> pointed to by
		/// <paramref name="ptr" />.
		/// </returns>
		public static ref T DeRef<T>(Pointer<T> ptr) where T : struct {

			return ref ptr.DeRef();

		}


		// TODO: Change to T : unmanaged when VB.Net supports the unmanaged constraint
		/// <summary>
		/// Creates a new <see cref="Pointer&lt;T&gt;" /> and initializes it with
		/// the address of a <see cref="System.ValueType" />.
		/// </summary>
		/// <remark>
		/// An alias  for <see cref="AddressOf&lt;T&gt;" /> as <c>AddressOf</c> is a
		/// Visual Basic.Net keyword.
		/// </remark>
		/// <param name="arg">
		/// The <see cref="System.ValueType" /> to take the address of.
		/// </param>
		/// <returns>
		/// A <see cref="Pointer&lt;T&gt;" /> pointing to <paramref name="arg" />.
		/// </returns>
		public static Pointer<T> VarPtr<T>(ref T arg) where T : struct {

			return PointerHelpers.AddressOf(ref arg);

		}

		// TODO: Change to T : unmanaged when VB.Net supports the unmanaged constraint
		/// <summary>
		/// Creates a new <see cref="Pointer&lt;T&gt;" /> and initializes it with
		/// the address of a <see cref="System.ValueType" />.
		/// </summary>
		/// <remark>
		/// An alias of this method is <see cref="VarPtr&lt;T&gt;" /> as
		/// <c>AddressOf</c> is a Visual Basic.Net keyword.
		/// </remark>
		/// <param name="arg">
		/// The <see cref="System.ValueType" /> to take the address of.
		/// </param>
		/// <returns>
		/// A <see cref="Pointer&lt;T&gt;" /> pointing to <paramref name="arg" />.
		/// </returns>
		public static Pointer<T> AddressOf<T>(ref T arg) where T : struct {

			Ldarg(nameof(arg));
			Conv_U();
			Newobj(new MethodRef(typeof(Pointer<T>), ".ctor", typeof(void *)));

			return IL.Return<Pointer<T>>();

		}

	}

}