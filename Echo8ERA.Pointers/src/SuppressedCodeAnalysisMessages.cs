using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", Justification = "The project's purpose is to implement pointers", Scope = "Module")]
[assembly: SuppressMessage("Microsoft.Naming", "CA2225:OperatorOverloadsHaveNamedAlternates", Justification = "The alternatives it looks for are just wierd", Scope = "Module", MessageId = "op_Implicit")]
[assembly: SuppressMessage("Microsoft.Naming", "CA2225:OperatorOverloadsHaveNamedAlternates", Justification = "The alternatives it looks for are just wierd", Scope = "Module", MessageId = "op_Explicit")]